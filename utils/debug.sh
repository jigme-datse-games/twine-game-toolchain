#!/bin/bash

cd ${BUILD_DIR}
VERSION=`ls -1dv [0123456789]* | tail -1`

cd ${VERSION}

source=${INDEXSOURCE}

cp "${source}" "${source}-orig"
cat "${source}" | sed "s/options=\"\"/options=\"debug\"/" > "${source}-debug"
cp "${source}-debug" "${source}"