#!/bin/bash

cd ${BUILD_DIR}
VERSION=`ls -1dv [0123456789]* | tail -1`

cd ${VERSION}
xdg-open "${INDEXSOURCE}"
