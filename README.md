# Twine Game Toolchain

This is the toolchain for Twine game development as I would like to have it setup.  It will be loaded as a sub repository for a Twine project.  

## License

Using GPL 2.0 or Later...  

## Usage 

To update use filename-local.extension to call the local version of this (I am doing that with the `start-work.sh` to have the settings for the current project).

* `start-work.sh`
  * This sets variables that other utilities use.
  * `source start-work.sh` to set your envioronment variables.
* `publish.sh` 
  * This will publish your project to itch.io with setting the correct values for the used variables.
* `new-version.sh`
  * Creates a new version, the latest version is automatically detected by other scripts such as `publish.sh` and `build.sh`
* `build.sh`
  * This builds a twine html file, in the latest version.
* `debug.sh`
  * Not tested recently, it sets the twine playable .html file to "debug".  This was used when using Tee-Wee editor, which at the time had no way to set this within the editor.
  * This has now been tested, it can be used just to update the latest build to a debug build. 

## Aditional Scripts

These are not part of the main workflow, and probably aren't strictly needed unless your workflow involves some of the things which created the need to have them here:

* `tweego-file-splitter.py`
  * This splits a .twee file into individual .twee files in the directory listed.
  * I use this when converting a Twine project created in Twine to a command line version.
* `archive.sh`
  * This creates a twine .html archive.  This can be imported into Twine.  
* `assets.sh`
  * This is used by the publish script, to move the assets from the assets folder, into the publish folder.